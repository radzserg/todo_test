**Design notes**

In this task I wanted to demonstrate my ability to work with some modern tools. I used React.    
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). 
I didn't used 3rd party libraries(like color picker) in order to demonstrate my code.

**Installation**

```bash
npm install
npm start

# for production build, review output instructions
npm run-script build
```

**Tests**

```bash
npm test
```

**Demo**

[https://youtu.be/KWaR6Pq0lwQ](https://youtu.be/KWaR6Pq0lwQ)