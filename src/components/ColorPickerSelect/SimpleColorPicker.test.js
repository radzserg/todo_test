import React from 'react';
import { shallow } from 'enzyme';
import SimpleColorPicker from './SimpleColorPicker';

it('renders color picker with default colors', () => {
  const wrapper = shallow(<SimpleColorPicker onSelect={() => null}/>);

  expect(wrapper.find("li").length).toEqual(8);
});

it('renders color picker with provided colors', () => {
  const wrapper = shallow(<SimpleColorPicker colors={['#fff', '#000']} onSelect={() => null}/>);

  expect(wrapper.find("li").length).toEqual(2);
});