import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './SimpleColorPicker.css';

export default class SimpleColorPicker extends Component {

  static propTypes = {
    colors: PropTypes.array,
    onSelect: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    const defaultColors = [
      '#000000',
      '#FF0000',
      '#00FF00',
      '#0000FF',
      '#FFFF00',
      '#00FFFF',
      '#FF00FF',
      '#C0C0C0'
    ];
    const colors = this.props.colors && this.props.colors.length ? this.props.colors : defaultColors;
    this.state = {
      colors
    }
  }

  render() {
    const options = this.state.colors.map((color, index) => {
      const style = {backgroundColor: color};

      return (
        <li key={index} onClick={() => this.props.onSelect(color)}>
          <div className={'color-option'} style={style}/>
        </li>
      );
    });
    return (
      <ul className='color-picker-select'>
        {options}
      </ul>
    );
  }
}

