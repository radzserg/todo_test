import React from 'react';
import { shallow } from 'enzyme';
import TodoList from './TodoList';
import TodoItems from './TodoItems/TodoItems';

it('does render Todo List with items', () => {
  const wrapper = shallow(<TodoList/>);
  expect(wrapper.contains('ToDo List')).toBe(true);
  expect(wrapper.find(TodoItems).exists()).toBe(true);
});