import React from 'react';
import { shallow } from 'enzyme';
import TodoItemForm from './TodoItemForm';

const dummy = () => { return null };

it('renders form for new item', () => {
  const wrapper = shallow(<TodoItemForm onChange={dummy} />);
  expect(wrapper.contains("Add Task")).toBe(true);
  expect(wrapper.find("input").prop('value')).toEqual('');
});

it('renders form for existing item', () => {
  const wrapper = shallow(<TodoItemForm onChange={dummy} value={'test'} />);
  expect(wrapper.contains("Save Task")).toBe(true);
  expect(wrapper.find("input").prop('value')).toEqual('test');
});