import React, { Component } from 'react'
import PropTypes from 'prop-types';

export default class TodoItemForm extends Component {

  static propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      value: this.props.value ? this.props.value : ''
    };
  }

  handleInputChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({ value: ''});
    this.props.onChange(this.state.value)
  }

  render() {
    const buttonLabel = this.props.value ? 'Save Task' : 'Add Task';

    return (
      <form onSubmit={this.handleSubmit}>
        <input placeholder="Task" onChange={this.handleInputChange} value={this.state.value} />
        <button type="submit">{buttonLabel}</button>
      </form>
    );
  }
}

