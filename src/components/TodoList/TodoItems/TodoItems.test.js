import React from 'react';
import { shallow } from 'enzyme';
import TodoItems from './TodoItems';
import TodoItem from './TodoItem/TodoItem';

const dummy = () => {};

it('does not render anything if items are not provided', () => {
  const wrapper = shallow(<TodoItems items={[]} onDeleteItem={dummy} onChangeItem={dummy}/>);

  expect(wrapper.isEmptyRender()).toBe(true);
});

it('does renders items', () => {
  const items = [
    {id: '1', text: 'Item 1'},
    {id: '2', text: 'Item 2'},
  ];
  const wrapper = shallow(<TodoItems items={items} onDeleteItem={dummy} onChangeItem={dummy}/>);

  expect(wrapper.find(TodoItem).length).toEqual(2);
});
