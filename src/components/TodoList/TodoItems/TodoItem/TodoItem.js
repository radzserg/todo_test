import React, { Component } from 'react'
import PropTypes from 'prop-types';
import SimpleColorPicker from '../../../ColorPickerSelect/SimpleColorPicker';
import TodoItemForm from '../../TodoItemForm/TodoItemForm';

export default class TodoItem extends Component {

  static propTypes = {
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    color: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.toggleSelectPicker = this.toggleSelectPicker.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.state = {
      colorPickerOpened: false
    }
  }

  handleChange(attrs) {
    this.props.onChange(this.props.id, attrs);
    this.setState({ colorPickerOpened: false });
  }

  toggleSelectPicker() {
    this.setState({
      colorPickerOpened: !this.state.colorPickerOpened
    })
  }

  render() {
    const color = this.props.color ? this.props.color : '#808080';
    const style = {"border": "2px solid " + color};

    return (
      <div className="item" style={style}>
        {!this.state.colorPickerOpened &&
          <div>
            {this.props.text}
            <div className={'delete-item'}>
              <button className={'edit-item'} onClick={this.toggleSelectPicker}>edit</button>
              <button onClick={() => this.props.onDelete(this.props.id)}>X</button>
            </div>
          </div>
        }
        {this.state.colorPickerOpened &&
          <div>
            <TodoItemForm id={this.props.id} value={this.props.text} onChange={(text) => {this.handleChange({ text })}} />
            <SimpleColorPicker onSelect={(color) => {this.handleChange({ color })}}/>
          </div>
        }
      </div>
    );
  }
}

