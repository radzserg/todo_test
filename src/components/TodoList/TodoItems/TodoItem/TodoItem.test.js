import React from 'react';
import { shallow } from 'enzyme';
import TodoItem from './TodoItem';
import TodoItems from '../TodoItems';
import SimpleColorPicker from '../../../ColorPickerSelect/SimpleColorPicker';
import TodoItemForm from '../../TodoItemForm/TodoItemForm';

const dummy = () => {};

it('renders single Todo item', () => {
  const wrapper = shallow(<TodoItem id='string1' text='Item 1' onChange={dummy} onDelete={dummy} />);
  expect(wrapper.contains('Item 1')).toBe(true);
  expect(wrapper.find('.item').prop('style').border).toEqual('2px solid #808080');
});

it('renders single Todo item with provided border color', () => {
  const wrapper = shallow(<TodoItem id='string1' text='Item 1' color='#fff' onChange={dummy} onDelete={dummy} />);
  expect(wrapper.contains('Item 1')).toBe(true);
  expect(wrapper.find('.item').prop('style').border).toEqual('2px solid #fff');
});

it('does not renders update fields in initial state', () => {
  const wrapper = shallow(<TodoItem id='string1' text='Item 1' color='#fff' onChange={dummy} onDelete={dummy} />);

  expect(wrapper.find(SimpleColorPicker).length).toEqual(0);
  expect(wrapper.find(TodoItemForm).length).toEqual(0);
});

it('renders update form when edit is clicked', () => {
  const wrapper = shallow(<TodoItem id='string1' text='Item 1' color='#fff' onChange={dummy} onDelete={dummy} />);

  wrapper.find('.edit-item').simulate('click');
  expect(wrapper.find(SimpleColorPicker).length).toEqual(1);
  expect(wrapper.find(TodoItemForm).length).toEqual(1);
});