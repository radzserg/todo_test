import React, { Component } from 'react'
import './TodoItems.css';
import TodoItem from './TodoItem/TodoItem';
import PropTypes from 'prop-types';

export default class TodoItems extends Component {

  static propTypes = {
    items: PropTypes.array.isRequired,
    onDeleteItem: PropTypes.func.isRequired,
    onChangeItem: PropTypes.func.isRequired,
  };

  render() {
    const items = this.props.items;
    if (!items.length) {
      return null;
    }
    const listItems = items.map((item) => {
      return (
        <li key={item.id}>
          <TodoItem
            id={item.id}
            text={item.text}
            color={item.color}
            onDelete={this.props.onDeleteItem}
            onChange={this.props.onChangeItem}
          />
        </li>
      );
    });

    return (
      <ul className="todo-items">
        {listItems}
      </ul>
    );
  }
}

