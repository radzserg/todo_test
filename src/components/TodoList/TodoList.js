import React, { Component } from 'react';
import TodoItems from './TodoItems/TodoItems';
import uniqueId from '../../lib/unique_id';
import PropTypes from 'prop-types';
import './TodoList.css';
import TodoItemForm from './TodoItemForm/TodoItemForm';

export default class TodoList extends Component {

  static propTypes = {
    items: PropTypes.array
  };

  constructor(props) {
    super(props);
    this.addNewTask = this.addNewTask.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.changeItem = this.changeItem.bind(this);

    this.state = {
      items: this.props.items ? this.props.items : []
    }
  }

  addNewTask(text) {
    if (text === '') {
      return;
    }

    const newItem = {id: uniqueId(), text };
    const items = [...this.state.items, newItem];

    this.setState({
      items: items
    })
  }

  deleteItem(id) {
    const filteredItems = this.state.items.filter(item => {
      return item.id !== id
    });
    this.setState({
      items: filteredItems,
    })
  }

  changeItem(id, attrs) {
    const items = this.state.items;
    const foundIndex = items.findIndex(item => item.id === id);

    items[foundIndex] = Object.assign({}, items[foundIndex], attrs);
    this.setState({ items });
  }

  render() {
    return (
      <div className="todo-list">
        <div className="header">
          ToDo List
          <TodoItemForm onChange={this.addNewTask}/>
          <TodoItems
            items={this.state.items}
            onDeleteItem={this.deleteItem}
            onChangeItem={this.changeItem}
          />
        </div>
      </div>
    );
  }
}

