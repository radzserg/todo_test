import React, { Component } from 'react';
import './App.css';
import TodoList from './components/TodoList/TodoList';

class App extends Component {
  render() {
    // test data
    const items = [
      {id: '1', text: 'Shopping', color: '#0f0'},
      {id: '2', text: 'Jogging'},
    ];
    return (
      <div className="App">
        <div className='todo-block'>
          <TodoList items={items}/>
        </div>
      </div>
    );
  }
}

export default App;
